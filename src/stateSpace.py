#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from threading import Thread
from pprint import pprint
import time


from datetime import datetime
from networkAPI import networkAPI

  


# pos is (row, column), not (x, y)
class Node:
    def __init__(self, val, pos):
        self.val = val
        # Position info is stored here and ALSO as index in mapka -
        # this is a form of data duplication, which is evil!
        self.pos = pos
        self.visited = False
    def __repr__(self):
        # nice repr for pprint
        return repr(self.pos)


class StateSpace():
    '''
    classdocs
    '''
    direction = 10
    shouldCompute = False
    mapka = []
    width = 20
    height = 12
    
    hasEnded = True
    
    moves = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    def __init__(self):
        '''
        Constructor
        '''
        Thread.__init__(self)
    
    def bfs(self, mapka, start):
        print("ASDASDAS DAS B F S ")
        fringe = [[start]]
        # Special case: start == goal
        #if start.val == '': #jidlo je na me pozici
        #    return [start]
        start.visited = True
        # Calculate width and height dynamically. We assume that "mapka" is dense.
        
        # List of possible moves: up, down, left, right.
        # You can even add chess horse move here!
                
        while fringe:            
            # Print fringe at each step
            #pprint(fringe)
            #print('')
            # Get first path from fringe and extend it by possible moves.
            path = fringe.pop(0)
            node = path[-1]
            posx = node.x
            posy = node.y
            print("while fringe "+str(posx)+" "+str(posy)+" "+str(node.type)+" "+str(node.canGrab()))
            # Using moves list (without all those if's with +1, -1 etc.) has huge benefit:
            # moving logic is not duplicated. It will save you from many silly errors.
            # The example of such silly error in your code:
            # mapka[pos[0-1]][pos[1]].visited = True
            #           ^^^
            # Also, having one piece of code instead of four copypasted pieces
            # will make algorithm much easier to change (e.g. if you want to move diagonally).
            for move in self.moves:
                print("MOVES")
                # Check out of bounds. Note that it's the ONLY place where we check it. Simple and reliable!
                if not (0 <= posx + move[0] < self.width and 0 <= posy + move[1] < self.height):
                    print("Cont")
                    continue
                print("1")
                neighbor = mapka[posy + move[1] + (posx + move[0])*self.height]
                print("2")
                
                if neighbor.canGrab(): #prida cestu
                    print("3")                    
                    if len(path) is 1:
                        print("HNED VEDLE")
                        self.direction = self.smer(move)
                    else:
                        self.direction = self.smerMeziNodama(path[0], path[1])
                    print("found grab")
                    #return path + [neighbor]
                    print(self.direction)
                    return self.direction 
                
                elif neighbor.canSeize() and not neighbor.visited:
                    print("CAN SEIZE")
                    neighbor.visited = True
                    fringe.append(path + [neighbor])  # creates copy of list    
                
                print("TISKNU "+ str(neighbor.canSeize())+str(neighbor.visited))            

        print("HEJA")


        raise Exception('Path not found!')

    def smerMeziNodama(self, prvni, druha):
        print(str(prvni.x)+" "+str(prvni.y))
        print(str(druha.x)+" "+str(druha.y))
        smery = [(-1, 0), (1, 0), (0, 1), (0, -1)]
        vektor = (druha.x - prvni.x, druha.y- prvni.y)
        if vektor == smery[0]:
            return 50
        elif vektor == smery[1]:
            return 30
        elif vektor == smery[2]:
            return 40
        elif vektor == smery[3]:
            return 20                
        else:
            return 30
        
    def smer(self, prvni):
        smery = [(-1, 0), (1, 0), (0, 1), (0, -1)]

        if prvni == smery[0]:
            return 50
        elif prvni == smery[1]:
            return 30
        elif prvni == smery[2]:
            return 40
        elif prvni == smery[3]:
            return 20                
        else:
            return 30      
        
    def run(self):
        while True:                 
            if self.shouldCompute:
                print("bezi vypocet")
                
                	#potrebuju
                try:        
                    vychozi = 0
                               
                    for i in range(len(self.mapka)):
                        if self.mapka[i].type == 20:
                            vychozi = i
                            break
                    print("VYCHOZI "+str(vychozi)+str(self.mapka[vychozi].x)+" "+str(self.mapka[vychozi].y))
                            
                            
                    
                    path = self.bfs(self.mapka, self.mapka[vychozi])
                    print("Path found: {!r}".format(path))
                    self.shouldCompute = False
                    
                except Exception as ex:
                    # Learn to use exceptions. In your original code, "no path" situation
                    # is not handled at all!
                    print(ex)
                    self.hasEnded = True
                    direction = 10
                self.hasEnded = True


                
    
    def cycleEnd(self):
        '''pauza vypoctu, ziskani nejlepsiho smeru'''
        print("KONEC CYKLU")
        self.shouldCompute = False
        self.hasEnded = False
        return self.direction
    
    def newCycle(self, mapka):
        '''spusteni vypoctu, nastaveni vychozych parametru'''
        print("NOVEJ CYKL")

        self.mapka = mapka 
        self.shouldCompute = True
        self.hasEnded = False


      
if __name__ == '__main__':
    pripojeni = networkAPI()
    vypocet = StateSpace()
    
    while True:
        mapka  = pripojeni.receive()
        
        
        vychozi = 0        
                           
        for i in range(len(mapka)):
            if mapka[i].type == 20:
                vychozi = i
                break
                       
        print("VYCHOZI "+str(mapka[i].x)+" "+str(mapka[i].y))    
        
        try:
            smer = vypocet.bfs(mapka, mapka[vychozi])
        except Exception as ex:
            print("VYJIMKA")
            print(ex)
            smer = 10
        
        pripojeni.send(smer)
        print("....")

    
            