import array


class InvalidOperation(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class TestFailed(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Stack:
    def __init__(self, capacity):
        print("Constructor stack")
        assert capacity > 0 
        self.size = 0
        self.capacity = capacity
        self.head = 0
        self.local_array = array.array('i', range(self.capacity))

    def __str__(self):
        out = ""
        for i in range(self.capacity):
            out += str(i) + ": " + str(self.local_array[i]) + "\n"
        return out
    
    def push(self, value):
        if(self.capacity == self.size):
            return False
        self.local_array[self.size] =  value
        self.size += 1
        return True

    def top(self):
        if self.size == 0:
            raise InvalidOperation("Cannot top from empty stack.")
        return self.local_array[self.size-1]

    def pop(self):
        if self.size == 0:
            raise InvalidOperation("Cannot pop from empty stack.")
        self.size -= 0
        return True

class StackUnitTest:
    def __init__(self, stack):
        self.stack = stack
        print("Beginning tests")
        self.runTests()

    def runTests(self):
        self.testPopFail()
        self.testPush()
        self.testTop()
        self.testPop()

    def testPush(self):
        result = stack.push(10)
        if not result:
            raise TestFailed("Unit test failed: testPush.")

    def testTop(self):
        result = self.stack.top()
        if result != 10:
            raise TestFailed("Unit test failed: testTop.")

    def testPop(self):
        result = stack.pop()
        if not result:
            raise TestFailed("Unit test failed: testPop.")

    def testPopFail(self):
        try:
            self.stack.pop()
        except InvalidOperation as e:
            print("Log: testPushFail raised an exception", e)

    
if __name__ == '__main__':
    stack = Stack(10)
    print("Stack initialiazed as:")
    print(stack)

    test = StackUnitTest(stack)

    print("Stack after tests:")
    print(stack)
