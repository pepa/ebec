#!/usr/bin/python3
# -*- coding: utf-8 -*-
from datetime import datetime
from stateSpace import StateSpace
from networkAPI import networkAPI

def main():
    pripojeni = networkAPI()
    
    
    vypocet = StateSpace()
    vypocet.daemon = True #nechte to tady, ukoncuje to hezky vlakno
    vypocet.start()
        

    print("PRED DATA")
    vypocet.newCycle(pripojeni.receive())
    start = datetime.now()
    print("PO DATA")
    

    while True:
        end = datetime.now()
        interval = end-start        
        if interval.seconds is 0 and interval.microseconds >= 3*100000: #1.5
            print(interval)            
            print("-  -- - - - - - - -- ")
            pripojeni.send(vypocet.cycleEnd())            
         
            while not vypocet.hasEnded:
                pass
            
            vypocet.newCycle(pripojeni.receive())
            
            print("TADY TD SE TO STANE")
            #break                
                
            start = datetime.now()

 

if __name__ == '__main__':
    main()