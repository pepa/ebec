#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from field import FieldCreator

creator = FieldCreator()

for i in range(66):
    field = creator.createNode(i, 0, 0)
    print(str(i), ":", field)
