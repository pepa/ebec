#!/usr/bin/env python3
# -*- coding: utf-8 -*-



class EnemyTrain():
    """"""
    def __init__(self, type, x, y):
        self.type = type
        self.x = x
        self.y = y
        self.visited = False

    def __str__(self):
        return "EnemyTrain"

    def canSeize(self):
        return False

    def canGrab(self):
        return False


class Food():
    """"""
    def __init__(self, type, x, y):
        self.type = type
        self.x = x
        self.y = y
        self.visited = False

    def __str__(self):
        return "Food"

    def canSeize(self):
        return True

    def canGrab(self):
        return True


class Wall():
    """"""
    def __init__(self, type, x, y):
        self.type = type
        self.x = x
        self.y = y
        self.visited = False

    def __str__(self):
        return "Wall"

    def canSeize(self):
        return False

    def canGrab(self):
        return False

class Empty():
    """"""
    def __init__(self, type, x, y):
        self.type = type
        self.x = x
        self.y = y
        self.visited = False

    def __str__(self):
        return "Empty"

    def canSeize(self):
        return True

    def canGrab(self):
        return False
        

class FieldCreator():
    """Vytvari objek Field.
       Pokud selze, vraci None."""

    def createNode(self, type, x, y):
        field = None
        
        if type == 0:
            field = Empty(0, x, y)
        elif type == 1:
            field = Wall(1, x, y)
        elif type == 2:
            field = Food(2, x, y)
        elif type % 10 == 0:
            field = EnemyTrain(type, x, y)
        elif type % 10 == 1:
            field = Wall(type, x, y)
        elif type % 10 == 5:
            field = Wall(type, x, y)
        return field