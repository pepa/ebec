#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import socket
from field import FieldCreator

class networkAPI:
	def __init__(self):
		self.connect()


	def connect(self):
		self.prijem = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self.prijem.connect(('192.168.38.161',1010))

		self.odeslani = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.odeslani.connect(('192.168.38.161', 1011))

	def receive(self):
		objArray = []
		creator = FieldCreator()
		i = 0
		prev_x = 0
		prev_y = 0
		x = 0
		y = 0
		while i < 240:
			data = self.odeslani.recv(1)
			if data:
				x = int.from_bytes(self.odeslani.recv(1), 'little')
				y = int.from_bytes(self.odeslani.recv(1), 'little')
				
				if (prev_y+1) < y:
					for j in range(0,(y-prev_y-1)):
						i = i + 1
						objArray.append(creator.createNode(0,x,prev_y+j+1))

				objArray.append(creator.createNode(int.from_bytes(data,'little'),x,y))	
				i = i + 1
				prev_y = y
				prev_x = x
		#for i in range(240):
		#	print(str(i),":",objArray[i])
		return objArray

	def send(self, message):
		intxt = str(message)
		testik =bytearray()
		testik.append(message)
		self.prijem.send(testik)

	def close(self):
		return False


if __name__ == '__main__':
	test = networkAPI()
	test.receive()
		
